#!/bin/sh

checkUrlFromFile(){
    # args:
    #   filename (from *-urlinfo.txt list)
    local filename="$1"
    export url="$(source "$filename" && echo $module)"
    echo -n "Checking URL '$url' ... "
    if curl --output /dev/null --silent --head --fail "$url"; then
        echo "OK"
        return 0
    else
        echo "Fail"
        return 1
    fi
}

gitbranch="$(git rev-parse --abbrev-ref HEAD)"

toolchain_list=""
if echo $gitbranch | grep -q "^LCG_"; then
  toolchain_list="$(echo $gitbranch | cut -d_ -f 2)"
else
  base_toolchains="hsf|dev3|dev4|dev3python3"
  toolchain_list="$(ls cmake/toolchain/heptools-* | grep -Ev "common|contrib" | grep -E "(${base_toolchains})\." | xargs -n 1 basename | sed 's/.*-\(.*\)\..*/\1/g')"
fi

BASEDIR="$PWD"
sourcedir="$BASEDIR"
for toolchain in ${toolchain_list}; do
  echo " Checking toolchain ${toolchain} ... "
  builddir="$BASEDIR/build_${toolchain}"
  installdir="$BASEDIR/install_${toolchain}"
  mkdir -p "$builddir"
  cd "$builddir"
  cmake -DLCG_VERSION=${toolchain} \
        -DCMAKE_INSTALL_PREFIX=$installdir \
        $sourcedir
  if [ $? -ne 0 ]; then
      echo "Configure failed. Exit."
      exit 1
  fi
  res=0
  echo "Checking all URLs ..."
  touch $BASEDIR/build_${toolchain}/missing.txt
  # find $builddir -name '*-urlinfo.txt' | 
  while read filename; do
      checkUrlFromFile "$filename"
      if [ $? -ne 0 ]; then
          # echo "URL check failed. Setting flag."
          echo $url >> $BASEDIR/build_${toolchain}/missing.txt
          res=1
      fi
  done < <(find $builddir -name '*-urlinfo.txt')
  if [ $res -ne 0 ]; then
    echo "Missing URLs:"
    cat $BASEDIR/build_${toolchain}/missing.txt
    exit 1
  fi
  echo "Checking empty hashes ..."
  python $sourcedir/cmake/scripts/check_hashes.py `ls $builddir/LCG_${toolchain}*.txt | grep -v contrib`
  if [ $? -ne 0 ]; then
    exit 1
  fi
done

