set(MCGENPATH  MCGenerators)

if(LCG_PYTHON_VERSION EQUAL 2)
  LCG_external_package(sherpa            2.2.8          ${MCGENPATH}/sherpa       author=2.2.8 hepevt=10000 openloops=2.1.1)
  LCG_external_package(sherpa-openmpi    2.2.8.openmpi3  ${MCGENPATH}/sherpa      author=2.2.8 hepevt=10000 openloops=2.1.1)
endif(LCG_PYTHON_VERSION EQUAL 2)

LCG_external_package(openloops         2.1.1          ${MCGENPATH}/openloops)
LCG_external_package(herwig3           7.2.0          ${MCGENPATH}/herwig++  thepeg=2.2.0 madgraph=2.6.7 openloops=2.1.1 lhapdf=6.2.3)

LCG_external_package(yoda              1.8.0          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             3.0.1          ${MCGENPATH}/rivet yoda=1.8.0 )

LCG_external_package(heputils          1.3.2          ${MCGENPATH}/heputils )

LCG_external_package(mcutils           1.3.4          ${MCGENPATH}/mcutils  heputils=1.3.2)

LCG_external_package(collier           1.2.4          ${MCGENPATH}/collier)
LCG_external_package(syscalc           1.1.7          ${MCGENPATH}/syscalc)

LCG_external_package(madgraph5amc      2.6.7          ${MCGENPATH}/madgraph5amc author=2.6.7)

LCG_external_package(lhapdf            6.2.3          ${MCGENPATH}/lhapdf       )

LCG_external_package(powheg-box-v2     r3043.lhcb    ${MCGENPATH}/powheg-box-v2 author=r3043  )

LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs	)
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           244            ${MCGENPATH}/pythia8 )

LCG_external_package(looptools         2.15           ${MCGENPATH}/looptools)

LCG_external_package(vbfnlo            3.0.0beta5     ${MCGENPATH}/vbfnlo feynhiggs=2.10.2)
LCG_external_package(FORM              4.1            ${MCGENPATH}/FORM)

LCG_external_package(njet              2.0.0          ${MCGENPATH}/njet)
LCG_external_package(qgraf             3.1.4          ${MCGENPATH}/qgraf)
LCG_external_package(gosam_contrib     2.0            ${MCGENPATH}/gosam_contrib)
LCG_external_package(gosam             2.0.4          ${MCGENPATH}/gosam)

LCG_external_package(thepeg            2.2.0          ${MCGENPATH}/thepeg )

LCG_external_package(tauola++          1.1.6          ${MCGENPATH}/tauola++     )

LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )

LCG_external_package(agile             1.5.0          ${MCGENPATH}/agile        )

LCG_external_package(photos++          3.61           ${MCGENPATH}/photos++     )

LCG_external_package(evtgen            1.7.0          ${MCGENPATH}/evtgen       tag=R01-07-00 pythia8=244 tauola++=1.1.6)

LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )

LCG_external_package(starlight         r313            ${MCGENPATH}/starlight   )

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )
if( NOT ${LCG_COMP} STREQUAL clang ) # Needs CLHEP
LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14 )
endif()
LCG_external_package(mctester          1.25.0         ${MCGENPATH}/mctester     )

LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmc              1.6.0          ${MCGENPATH}/crmc         )
# LCG_external_package(crmc              1.7.0          ${MCGENPATH}/crmc         )  
# ^-- Fails with "multiple definition of `pho_phist_'", no response from authors

LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )

LCG_external_package(professor         2.3.2          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

LCG_external_package(log4cpp           2.8.3p1         ${MCGENPATH}/log4cpp     )

LCG_external_package(rapidsim          1.4.4          ${MCGENPATH}/rapidsim     )

LCG_external_package(superchic         3.06          ${MCGENPATH}/superchic    )

if(NOT ${LCG_OS} MATCHES ubuntu|mac)
LCG_external_package(hoppet              1.1.5          ${MCGENPATH}/hoppet        )
endif()
if(NOT ${LCG_OS} MATCHES mac)
LCG_external_package(hto4l               2.02           ${MCGENPATH}/hto4l         )
LCG_external_package(prophecy4f          2.0.1          ${MCGENPATH}/prophecy4f    )
endif()

LCG_external_package(ampt                2.26t9b_atlas  ${MCGENPATH}/ampt author=2.26t9b )

LCG_external_package(recola_SM           2.2.2          ${MCGENPATH}/recola_SM      )
LCG_external_package(recola              2.2.0          ${MCGENPATH}/recola         )
