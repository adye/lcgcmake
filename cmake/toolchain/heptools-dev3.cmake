#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    HEAD     GIT=http://root.cern.ch/git/root.git         )
LCG_external_package(hepmc3  HEAD     GIT=https://gitlab.cern.ch/hepmc/HepMC3.git  )
LCG_external_package(DD4hep  master   GIT=https://github.com/AIDASoft/DD4hep.git   )
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu18|mac[0-9]+)
  LCG_AA_project(CORAL master)
  LCG_AA_project(COOL master)
endif()
LCG_remove_package(Gaudi)
LCG_external_package(madgraph5amc      3.0.1.beta     ${MCGENPATH}/madgraph5amc author=3.0.1.beta)
LCG_external_package(herwig3           7.2.0          ${MCGENPATH}/herwig++  thepeg=2.2.0 madgraph=3.0.1.beta openloops=2.1.1 lhapdf=6.2.3)
LCG_external_package(pythia8           244            ${MCGENPATH}/pythia8 )
LCG_external_package(thep8i            2.0.0          ${MCGENPATH}/thep8i  )
LCG_external_package(pythia6           429.2.lhcb     ${MCGENPATH}/pythia6    author=6.4.28 hepevt=200000) # new hepevt=200000
LCG_external_package(photos++          3.61.lhcb      ${MCGENPATH}/photos++   author=3.61  ) # patch for hepevt=200000
