#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    v6-20-00-patches  GIT=http://root.cern.ch/git/root.git )
LCG_external_package(hepmc3  3.1.1  )
