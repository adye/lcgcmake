#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)
 
LCG_external_package(ROOT    v6-20-00-patches   GIT=http://root.cern.ch/git/root.git )
LCG_external_package(hepmc3  3.1.1                                                   )

# Latest tagged version of COOL, CORAL instead of master (if required)
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu18|mac[0-9]+)
  LCG_remove_package(CORAL)
  LCG_AA_project(CORAL 3_2_2)
  LCG_remove_package(COOL)
  LCG_AA_project(COOL 3_2_2)
endif()


LCG_remove_package(Python)
LCG_external_package(Python 3.8.0)

# root_numpy is depricated in favour of uproot; doesn't work with 3.8
LCG_remove_package(root_numpy)
