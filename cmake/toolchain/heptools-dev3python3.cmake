#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )
LCG_external_package(DD4hep        master   GIT=https://github.com/AIDASoft/DD4hep.git   )
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu18|mac[0-9]+)
  LCG_AA_project(CORAL master)
  LCG_AA_project(COOL master)
endif()
LCG_remove_package(Gaudi)
if(APPLE)    # We cannot build ROOT (HEAD) and R together because RConfig.h file clashes 
  LCG_remove_package(R)
  LCG_remove_package(rpy2)
endif()
