#---External packages----------------------------------------------
include(heptools-dev-base)
#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

#---Overwrites-----------------------------------------------------
LCG_external_package(ROOT           6.18.04 )
LCG_external_package(hepmc3         3.0.0   )

if(${LCG_OS} MATCHES mac)  # AppleClang is more strict on C++17 deprecated functionality
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(dire4pythia8)
  LCG_remove_package(vincia)
  LCG_remove_package(herwig3)
  LCG_remove_package(ampt)
  LCG_remove_package(unigen)
endif()