# How to create a new set of externals for Geant4

1. Clone the lcgcmake repository and add the `lcgcmake/bin` into the PATH:
    ```
    git clone https://gitlab.cern.ch/sft/lcgcmake.git
    export PATH=$PWD/lcgcmake/bin:$PATH
    ```

2. Create a new toolchain file in `lcgcmake/cmake/toolchain`
   ```
    cd lcgcmake/cmake/toolchain
    cp heptools-geant4ext<OLD>.cmake heptools-geant4extYYYYMMDD.cmake
    ```

3. Edit heptools-geant4extYYYYMMDD.cmake taking recent versions from heptools-dev-base.cmake

3. Test locally the new version of the geant4ext stack

   - Setup the compiler you wish to use (gcc7 in this case)
     ```
     source /cvmfs/sft.cern.ch/lcg/contrib/gcc/7binutils/x86_64-centos7/setup.sh
     ```

    - Create a build area
      ```
      mkdir build
      cd build
      ```

    - Configure and install
      ```
      lcgcmake config --version geant4extYYYYMMDD --prefix ../install
      lcgcmake install Geant4-externals

      ```
    - Fix inconsistencies, missing packages, etc. in heptools-geant4extYYYYMMDD.cmake until it builds correctly until the end

4. Commit the new file heptools-geant4extYYYYMMDD.cmake to lcgcmake repository or creare a MR

5. Update the configuration of the Jenkins project [lcg_geant4ext_build](https://epsft-jenkins.cern.ch/view/LCG%20Externals/job/lcg_geant4ext_build/)
   - update `LCG_VERSION, SLOT` with the new version geant4extYYYYMMDD
   - add the required Compilers and OSs
   - set the default `Combinations` (in advanced)

6. Launch the Jenkins job [lcg_geant4ext_build](https://epsft-jenkins.cern.ch/view/LCG%20Externals/job/lcg_geant4ext_build/)
   - If a combination fails can be re-launched after having fix the corresponding issue.
   - Details of the build can also be found at [CDash](http://cdash.cern.ch/index.php?project=LCGSoft) under the section 'releases'


